<!doctype html>
<html lang="pl">
    <head>
        <link rel="stylesheet" href="{{asset('/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('/css/app.css')}}">
        <link rel="stylesheet" href="{{asset('/css/jquery-ui.css')}}">
        <script src="{{asset('/js/jquery.min.js')}}"></script>
        <script src="{{asset('/js/popper.min.js')}}"></script>
        <script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src = "{{asset('/js/jquery-1.12.4.js')}}" ></script>
        <script src="{{asset('/js/jquery-ui.js')}}"></script>
        </head>
        <body>
            <div class='container strava_segments'>
                <div class="row">
                    <div class='offset-md-3 col-md-6'>
                        <h3>
                            Strava activities
                        </h3>
                    </div>    
                </div>
                <div class='row'>
                    <div class='offset-md-3 col-md-6'>
                        <form>
                            <div class="form-group search_top">
                                <input type="text" class="form-control" name="activity_name" id="search_path" aria-describedby="path" placeholder="Enter path">
                                <button disabled type="submit" class="btn btn-primary submit_search">
                                    Search
                                </button>
                                <button  type="reset" class="btn btn-danger reset_search">
                                    Reset
                                </button>
                            </div>
                        </form>    
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-12'>
                        <table class="table table-bordered table-sm table-activities">
                            <tr>
                                <th>
                                    Name:
                                </th>
                                <th>
                                    Moving time:
                                </th>
                                <th>
                                    Elapsed time:
                                </th>
                                <th>
                                    Average heartrate:
                                </th>
                                <th>
                                    Max heartrate:
                                </th>
                                <th>
                                    Date:
                                </th>
                                <th>
                                    Activity ID:
                                </th>
                            </tr>

                            @foreach ($results as $item)
                            <tr>
                                <td>
                                    {{$item['name']}}
                                </td>
                                <td>
                                    {{$item['moving_time']}}
                                </td>
                                <td>
                                    {{$item['elapsed_time']}}
                                </td>
                                <td>
                                    {{$item['average_heartrate']}}
                                </td>
                                <td>
                                    {{$item['max_heartrate']}}
                                </td>
                                <td>
                                    {{$item['start_date_local']}}
                                </td>
                                <td>
                                    {{$item['original_id']}}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <nav aria-label="Page navigation example">
                            @if(count($pages))
                                <ul class="pagination">
                                    @if( $current_page != 1 )
                                        <li class="page-item">
                                            <a class="page-link" href="{{route('strava.activities',['page'=>$prev_page_url,'activity_name'=>$activity_name])}}">
                                                Previous
                                            </a>
                                        </li>
                                    @endif
                                    
                                    @if( $pages[0] > 1 )
                                        @for ($i = 0; $i < $pages[0]; $i++)
                                            @if( $current_page - 1 == $i )
                                                <li class="page-item active">
                                                    <a class="page-link" href="{{route('strava.activities',['page'=>$i + 1,'activity_name'=>$activity_name])}}">
                                                        {{$i+1}}
                                                    </a>
                                                </li> 
                                            @else
                                                <li class="page-item">
                                                    <a class="page-link" href="{{route('strava.activities',['page'=>$i + 1,'activity_name'=>$activity_name])}}">
                                                        {{$i+1}}
                                                    </a>
                                                </li> 
                                            @endif
                                        @endfor
                                    @endif    

                                    @if( $current_page  != $pages[0] )      
                                        <li class="page-item">
                                            <a class="page-link" href="{{route('strava.activities',['page'=>$next_page_url,'activity_name'=>$activity_name])}}">
                                                Next 
                                            </a>
                                        </li>
                                    @endif    
                                </ul>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </body>
        <script>
            
        const specialChars = { 
            '&amp;' : '&'  ,
            '&quot;' : '"' ,
            '&#039;' : "'" ,
            '&lt;' : '<'   ,
            '&gt;' : '>' 
        };
        const specialCharsKeys = Object.keys(specialChars);
        
        const replaceSpecChar = (arr) => {
            
            const newArr = [];
            
            arr.forEach( (item,key) => {
                
                let elem = '';
                elem = item;
                
                for( let i = 0; i < specialCharsKeys.length; i++ ){
                    if( elem.indexOf(specialCharsKeys[i]) > -1 ){
                        let char = ''; 
                        char = new RegExp(specialCharsKeys[i], "g"); 
                        elem = elem.replace(char,specialChars[specialCharsKeys[i]]);
                    }
                }
                
                newArr.push(elem)
                
            });
            
            return newArr; 
        }
        
        
             
        $(function(){
            var availableTags = [ 
                @foreach ($autocomplete_array as $key => $item)
                    "{{$item['name']}}",
                @endforeach 
            ];
            
            const newArr = replaceSpecChar(availableTags);
            
            
            $("#search_path").autocomplete({
                source: newArr, 
                select: function (e, ui) {
                    $('.submit_search').prop( "disabled", false );
                },
                minLength: 3
            }); 
            
         });
         
         
    </script>
</html>


