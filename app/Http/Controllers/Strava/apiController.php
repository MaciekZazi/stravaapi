<?php

namespace App\Http\Controllers\Strava;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use App\StravaSegment as Segment;

class apiController extends Controller {
    
    private $guzzle_client = '';
    private $client_id = 0;
    private $client_secret = '';
    private $auth_url = 'http://www.strava.com/api/v3/oauth/authorize';
    private $api_url_list_activities = 'https://www.strava.com/api/v3/athlete/activities';
    private $api_url_single_activity = 'https://www.strava.com/api/v3/activities/';
    private $access_token_url = 'https://www.strava.com/api/v3/oauth/token';
    private $callback_url = 'http://laravelapi.tescik.ovh';
    private $code = '';
    private $response_type = 'code';
    private $grant_type = 'authorization_code';
    private $approval_prompt = 'force';
    private $scope = 'activity:read_all';
    private $access_token = '';
    private $refresh_token = '';
    private $per_page = 80;
    private $segment;
    
    public function __construct(Segment $segment){
        $this->guzzle_client = new Client();
        $this->segment = $segment;
    }
    
    public function authenticated(){
        //session(['access_token' => 'www' ]);
        //Session::forget('access_token');
        
        $this->setAccessToken();
        
        if($this->access_token != ''){
            echo "Token exist: {$this->access_token}";
        }else{
            echo "Token not exist or expired";
        }
        
    }//authenticated user with stored access token in session
    
    public function userAuth() {
         
        if (\Request::has('code') && !empty(\Request::get('code'))  ) {
            $this->code = \Request::get('code');
            $this->saveTokens();
        } else {
            $this->getCode();
        }

    }//authenticate to strava API and get tokens ( access and refresh )
    
    private function getCode(){
        
        redirect()->away(
                $this->auth_url . "?"
                . "client_id={$this->client_id}&"
                . "response_type={$this->response_type}&"
                . "redirect_uri={$this->callback_url}&"
                . "approval_prompt={$this->approval_prompt}&"
                . "scope={$this->scope}")
        ->send();
                
    }
    
    private function saveTokens(){
       
        try{
            $response = $this->guzzle_client->request('POST', $this->access_token_url, [
                'query' => [
                    'client_id' => $this->client_id,
                    'client_secret' => $this->client_secret,
                    'code' => $this->code,
                    'grant_type' => $this->grant_type
                ]
            ]);
            
            $body = json_decode($response->getBody(), true);

            if(!empty($body['access_token']) && !empty($body['refresh_token'])) {
                session([
                    'access_token' => $body['access_token'],
                    'refresh_token' => $body['refresh_token']
                ]);
            }   
            
            redirect()->away('/strava')->send();
            
        }catch(ClientException $e){
            throw new \Exception(Psr7\Message::toString($e->getResponse()));
        }
         
    }//save tokens to sesssion
    
    public function saveActivities(){
        
        $this->setAccessToken();
        $page = \Request::get('page');
        
        if($this->access_token != '' && \Request::has('page') && !empty($page) ){
            try{

                $response_activities = $this->guzzle_client->request(
                    'GET',
                    $this->api_url_list_activities."?page={$page}&per_page={$this->per_page}",
                    ['headers' =>
                        [
                            'Authorization' => "Bearer {$this->access_token}",
                            'Accept' => 'application/json'
                        ]
                    ]
                )->getBody()->getContents();

                $activities_list = array_reverse(json_decode($response_activities,true));
                
                //dd($activities_list);
                
                if(count($activities_list)){
                    
                    foreach($activities_list as $key => $item){
                        $id_activity = $item['id'];
                        $activity = $this->getActivity($id_activity);
                        $this->segment->saveSegment($activity,$id_activity);
                    }//loop for all activities with id
                    
                }   

            }catch(ClientException $e){
                throw new \Exception(Psr7\Message::toString($e->getResponse()));
            }    
        }else{
            echo "Token expired or You didn't put page number as parameter";
        }    
          
    }
    
    private function getActivity($id_activity){
        
        try{
            
            $response_activity = $this->guzzle_client->request(
                'GET',
                $this->api_url_single_activity.$id_activity,
                ['headers' =>
                    [
                        'Authorization' => "Bearer {$this->access_token}",
                        'Accept' => 'application/json'
                    ]
                ]
            )->getBody()->getContents();

            $activity = json_decode($response_activity,true);//get single activity
            
            return $activity;

        }catch(ClientException $e){
            throw new \Exception(Psr7\Message::toString($e->getResponse()));
        }
        
    }
    
    public function saveSingleActivity(){
        
        $this->setAccessToken();
        $id_activity = intval(\Request::get('id')); 
        
        if($this->access_token != '' && \Request::has('id') && !empty($id_activity) ){
            $activity = $this->getActivity($id_activity);
            $this->segment->saveSegment($activity,$id_activity);
        }else{
            echo "Token not exist or expired";
        }
        
    }
    
    private function setAccessToken(){
        if(Session::get('access_token') != ''){
            $this->access_token = Session::get('access_token');
        }
    }//store access token to session
    
    
}
