<?php

namespace App\Http\Controllers\Strava;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;
use App\StravaSegment as Segment;

class activitiesController extends Controller
{   
    
    private $segment;
    
    public function __construct(Segment $segment){
        $this->segment = $segment;
    }
    
    public function showActivities(){
        
        $res = [];
        $current_page = 1;
        $next_page_url = 2;
        $prev_page_url = 1;
        $activity_name = '';
        $pages = [];
        $autocomplete_arr = [];
        
        if(\Request::has('activity_name') && !empty(\Request::get('activity_name')) ){
            $activity_name = \Request::get('activity_name');
            list($res,$current_page,$total) = $this->segment->getSegments($activity_name,10);
            $pages[0] =  (int) ceil($total / 10);
        }
        
        $autocomplete_arr = $this->segment->getAllSegments();
        
        
        return view('strava.activities', [
            'results' => $res,
            'current_page' => $current_page,
            'next_page_url' => $current_page + 1,
            'prev_page_url' => $current_page - 1,
            'pages' => $pages,
            'activity_name' => $activity_name,
            'autocomplete_array' => $autocomplete_arr
        ]);
        
    }  
    
}
