<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StravaSegment extends Model
{
    protected $table = 'strava_segments';
    protected $fillable = ['id','name','moving_time','elapsed_time','max_heartrate','average_heartrate',
    'start_date_local','original_id'];
    
    public function getSegments($activity_name,$limit) {
         
            $segments = StravaSegment::
                                    where('name','=',$activity_name)
                                    ->orderBy('name')
                                    ->orderBy('moving_time')
                                    ->paginate(10);
        
        $segments_arr = $segments->toArray();
        $result = $this->getFormattedSegments($segments_arr);
      
        return [
            $result,
            $segments_arr['current_page'],
            $segments_arr['total']
        ];
    }
    
    public function getAllSegments(){
        return StravaSegment::distinct()->get('name')->toArray();
    }
    
    public function saveSegment($activity,$id_activity){
        
        foreach ($activity as $key2 => $item2) {
            
            if(!$this->checkActivityExists($id_activity)){
                if ($key2 == 'segment_efforts' && count($item2)) {
                    foreach ($item2 as $key3 => $segment) {
                        $strava = new StravaSegment();
                        $strava->name = isset($segment['name']) ? $segment['name'] : '';
                        $strava->original_id = $id_activity;
                        $strava->moving_time = isset($segment['moving_time']) ? $segment['moving_time'] : '';
                        $strava->elapsed_time = isset($segment['elapsed_time']) ? $segment['elapsed_time'] : '';
                        $strava->average_heartrate = isset($segment['average_heartrate']) ? $segment['average_heartrate'] : '';
                        $strava->max_heartrate = isset($segment['max_heartrate']) ? $segment['max_heartrate'] : '';
                        $strava->start_date_local = isset($segment['start_date_local']) ? $segment['start_date_local'] : '';
                        $strava->save();
                    }//loop for all segments in each activity
                }
            }
        }
        
    }
    
    private function checkActivityExists($id){
        
        if(StravaSegment::where('original_id', '=', $id)->first() === null){
            return false;
        }
        
        return true;
    }
    
    private function getFormattedSegments($all_segments){
        
        $result = [];
        
        if(is_array($all_segments['data']) && count($all_segments['data'])){
            foreach($all_segments['data'] as $key => $item){
                $result[$key]['name'] = $item['name'];
                $result[$key]['moving_time'] = $this->getMinutes($item['moving_time']);
                $result[$key]['elapsed_time'] = $this->getMinutes($item['elapsed_time']);
                $result[$key]['average_heartrate'] = $item['average_heartrate'];
                $result[$key]['max_heartrate'] = $item['max_heartrate'];
                $result[$key]['start_date_local'] = $item['start_date_local'];
                $result[$key]['original_id'] = $item['original_id'];
            }
        }
        
        return $result;
        
    }
    
    private function getMinutes($item){
        
        $integerVal = intval(floor($item / 60));
        $secondsVal = $item - ($integerVal * 60);
        
        $result = $integerVal.' minutes, '.$secondsVal.' seconds';
        
        return $result;
        
    }
    
}
