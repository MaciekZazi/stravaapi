<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Strava
Route::get('/', 'HomeController@redirect');

Route::namespace('Strava')->group(function () {
    Route::get('/strava-auth', 'apiController@userAuth');//get token
    Route::get('/strava', 'apiController@authenticated');//check token exists
    Route::get('/save-all-activities', 'apiController@saveActivities');//save all activities ( by page number as paramater )
    Route::get('/save-single-activity', 'apiController@saveSingleActivity');//save single activity by id as parameter
    Route::get('/strava-activities', 'activitiesController@showActivities')->name('strava.activities');//list activities from db
});

