<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StravaSegments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strava_segments', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_polish_ci';
            $table->bigIncrements('id');
            $table->bigInteger('original_id');//activity id w aktywnosci
            $table->timestamp('start_date_local', $precision = 0);//w segmencie
            $table->string('name');//segment name
            $table->integer('moving_time');
            $table->integer('elapsed_time');
            $table->string('max_heartrate');
            $table->string('average_heartrate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strava_segments');
    }
}
